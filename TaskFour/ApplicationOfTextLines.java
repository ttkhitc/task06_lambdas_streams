package TaskFour;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ApplicationOfTextLines {

	public static String enterText(BufferedReader reader) throws IOException {
//       User enter his text
		System.out.println("Enter few line of text. If you want to end enter empty line");
		String allText = "";

		for (int i = 0; i >= 0; i++) {

			String a = reader.readLine();

			if (a.isEmpty()) {
				break;
			} else {

				allText += a + " ";
			}
		}
		return allText;
	}

	public static void manipulationsWithText(String allText) {

//		Manipulations from task

		List<String> text = Arrays.asList(allText.replaceAll("[^A-Za-z0-9]", " ").split(" "));

		// Number of unique words

		long a = text.stream().distinct().count();
		System.out.println("Number of unique words = " + a);

		// List of unique words

		List<String> uniqueWords = text.stream().distinct().sorted().collect(Collectors.toList());
		System.out.println("List of unique words: " + uniqueWords);

//         Occurrence number of each word in the text

		Map<String, Long> count = text.stream()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		System.out.println("  Occurrence number of each word in the text: " + count);

//		Occurrence number of each symbol

		Map<Object, Long> s = allText.chars().mapToObj(c -> String.valueOf((char) c)).filter(str -> !str.equals(" "))
				.collect(Collectors.groupingBy(ch -> ch, Collectors.counting()));

		System.out.println("Occurrence number of each symbol:" + s);
	}

	public static void main(String[] args) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try {
			manipulationsWithText(enterText(reader));

		} catch (IOException e) {

			System.err.println("Problem in :" + e);

			e.printStackTrace();
		} finally {

			try {

				reader.close();

			} catch (IOException e) {

				e.printStackTrace();
			}
		}
	}
}
