//..................Task 1...........
public class ImplementationOfLambda {
	public interface MyInterface {

		public int myMethod(int a, int b, int c);

	}

	public static void main(String[] args) {

//         Average of three numbers
		MyInterface result = (a, b, c) -> (a * b * c / 3);

//		  Max of three numbers
		MyInterface result1 = (a, b, c) -> (Math.max(Math.max(a, b), c));

		System.out.println(result.myMethod(12, 6, 356));

		System.out.println(result1.myMethod(43, 55, 1234));

	}
}
