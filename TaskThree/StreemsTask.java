package TaskThree;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreemsTask {

	public static List<Integer> random1(Random random) {

		List<Integer> first = new ArrayList<Integer>(random.ints(0, 300).limit(20).boxed().collect(Collectors.toList()));

		return first;

	}

	public static List<Integer> random2(Random random) {

		List<Integer> second = Stream.iterate(0, n -> n + random.nextInt(30)).limit(20).collect(Collectors.toList());

		return second;

	}

	public static void calculations(List<Integer> s) {

//		 Calculate max value
		OptionalInt max = s.stream().mapToInt(x -> x).max();

		System.out.println("max = " + max);

//		Calculate min value
		OptionalInt min = s.stream().mapToInt(x -> x).min();

		System.out.println("min = " + min);

//		Calculate average
		OptionalDouble avg = s.stream().mapToInt(x -> x).average();

		System.out.println("average = " + avg);

//		Calculate sum of elements
		int sum = s.stream().mapToInt(x -> x).sum();

		System.out.println("sum = " + sum);

//		Count values bigger than average
		long countAvg = s.stream().filter(x -> x > avg.getAsDouble()).count();

		System.out.println("Count of values bigger than average = " + countAvg);
	}

	public static void main(String[] args) {

		Random random = new Random();

		calculations(random1(random));

	}
}
